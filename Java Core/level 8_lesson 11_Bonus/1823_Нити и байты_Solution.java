package com.javarush.task.task18.task1823;

import java.io.*;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/* 
Нити и байты
*/

public class Solution {
    public static Map<String, Integer> resultMap = new HashMap<String, Integer>();

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename;
        while (true) {
            filename = reader.readLine();
            if (filename.equals("exit")) break;
            else new ReadThread(filename).start();
        }
        reader.close();
    }

    public static class ReadThread extends Thread {
        public ReadThread(String fileName){
            super.setName(fileName);
        }

        public void run() {
            BufferedInputStream inputStream = null;
            try {
                inputStream = new BufferedInputStream(new FileInputStream(super.getName()));
                byte[] array = new byte[inputStream.available()];
                inputStream.read(array);
                int[] ASCII = new int[256];
                int maxcount = 0;
                int maxbite = 0;
                for (int i = 0; i < array.length; i++) {
                    if (array[i] > 0) {
                        ASCII[array[i]]++;
                        if (ASCII[array[i]] > maxcount) {
                            maxcount = ASCII[array[i]];
                            maxbite = array[i];
                        } else {
                            ASCII[array[i] + 127]++;
                            if (ASCII[array[i] + 127] > maxcount) {
                                maxcount = ASCII[array[i] + 127];
                                maxbite = i;
                            }
                        }
                    }
                }
                synchronized (resultMap){
                    resultMap.put(super.getName(), maxbite);
                }
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}