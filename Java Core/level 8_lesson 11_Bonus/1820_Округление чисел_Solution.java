package com.javarush.task.task18.task1820;

/* 
Округление чисел
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(reader.readLine()));
        BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(reader.readLine()));

        byte[] arrayIN = new byte[inputStream.available()];
        inputStream.read(arrayIN);
        String number = "";
        for (int i = 0; i < arrayIN.length; i++) {

            //если не возврат каретки (13), не новая строка (10) и не пробел (32), то формируем число из символов
            if (!(arrayIN[i] == 13 || arrayIN[i] == 10 || arrayIN[i] == 32)) {
                number += (char)arrayIN[i];
            }

            //иначе число парсим как Float, округляем и сохраняем в строку + пробел, которую побайтово заносим в массив
            else {
                String buffer = String.valueOf(Math.round(Float.parseFloat(number)))+" ";
                outputStream.write(buffer.getBytes(), 0, buffer.getBytes().length);
                number = ""; //чистим переменную под новое число
            }
        }

        //запись последнего числа из переменной
        String buffer = String.valueOf(Math.round(Float.parseFloat(number)));
        outputStream.write(buffer.getBytes(), 0, buffer.getBytes().length);

        inputStream.close();
        outputStream.close();
    }
}