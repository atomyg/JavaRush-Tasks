package com.javarush.task.task18.task1825;

import java.io.*;
import java.util.*;

/* 
Собираем файл
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        List<String> fileParts = new ArrayList<>();
        String name;
        while (true){
            name = reader.readLine();
            if (name.equals("end")) break;
            else{
                fileParts.add(name);
            }
        }
        Collections.sort(fileParts);
        int index = fileParts.get(1).indexOf(".part");
        name = fileParts.get(1).substring(0, index);
        BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(name));
        for (String part:fileParts) {
            BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(part));
            byte[] bytes = new byte[inputStream.available()];
            inputStream.read(bytes);
            outputStream.write(bytes, 0, bytes.length);
            inputStream.close();
        }
        outputStream.close();
    }
}