package com.javarush.task.task18.task1816;

/* 
Английские буквы
*/

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(args[0]));
        byte[] arr = new byte[inputStream.available()];
        inputStream.read(arr);
        int count = 0;
        //ASCII коды английских букв - прописные\большие(65-90) и строчные\маленькие (97-122)
        for (byte b : arr) {
            if (((b >= 65) && (b <= 90)) || ((b >= 97) && (b <= 122))) count++;
        }
        System.out.println(count);
        inputStream.close();
    }
}