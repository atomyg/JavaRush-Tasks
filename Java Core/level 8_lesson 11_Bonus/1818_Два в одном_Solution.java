package com.javarush.task.task18.task1818;

/* 
Два в одном
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        FileOutputStream fileOutputStream = new FileOutputStream(reader.readLine());
        FileInputStream fileInputStream1 = new FileInputStream(reader.readLine());
        FileInputStream fileInputStream2 = new FileInputStream(reader.readLine());

        byte[] array = new byte[fileInputStream1.available()];
        fileInputStream1.read(array);
        fileOutputStream.write(array, 0, array.length);
        fileInputStream1.close();

        array = new byte[fileInputStream2.available()];
        fileInputStream2.read(array);
        fileOutputStream.write(array, 0, array.length);
        fileInputStream2.close();
        fileOutputStream.close();
    }
}