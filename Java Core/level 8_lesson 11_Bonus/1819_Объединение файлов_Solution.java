package com.javarush.task.task18.task1819;

/* 
Объединение файлов
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String file1 = reader.readLine();
        String file2 = reader.readLine();

        //создаем временный поток чтения из 1-го файла и читаем его в массив temp
        FileInputStream fileInputStream = new FileInputStream(file1);
        byte[] temp = new byte[fileInputStream.available()];
        fileInputStream.read(temp);

        fileInputStream.close(); //закрываем временный поток чтения из 1-го файла
        fileInputStream = new FileInputStream(file2); //создаем поток чтения из 2-го файла
        FileOutputStream fileOutputStream = new FileOutputStream(file1); //создаем поток записи в 1-й файл

        //Читаем из 2-го файла в массив array и пишем в 1-й файл, потом пишем следом из массива temp
        byte[] array = new byte[fileInputStream.available()];
        fileInputStream.read(array);
        fileOutputStream.write(array, 0 , array.length);
        fileOutputStream.write(temp, 0, temp.length);

        fileInputStream.close();
        fileOutputStream.close();
    }
}