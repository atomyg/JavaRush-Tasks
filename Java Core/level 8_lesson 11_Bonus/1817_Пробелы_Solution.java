package com.javarush.task.task18.task1817;

/* 
Пробелы
*/

import java.io.FileInputStream;
import java.io.IOException;
import java.text.DecimalFormat;

public class Solution {
    public static void main(String[] args) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(args[0]);
        byte[] array = new byte[fileInputStream.available()];
        int count = fileInputStream.read(array);
        int space = 0;
        for (byte b:array) {
            if ((char)b == ' ') space++;
        }
        float f = (float)space/count*100;
        DecimalFormat df = new DecimalFormat("#.##");
        System.out.println(df.format(f));
        fileInputStream.close();
    }
}