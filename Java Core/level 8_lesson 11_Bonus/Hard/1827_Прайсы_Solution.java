package com.javarush.task.task18.task1827;

/* 
Прайсы
*/

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        String fileName = scanner.nextLine();
        scanner.close();

        if (args.length != 0 && args[0].equals("-c")) {
            String productName = "";
            float price = 0;
            int quantity = 0;

            //разделяем входную строку параметров на цену, кол-во и названиие с обрезанием длины
            for (int i = 1; i < args.length; i++) {
                try {
                    //если нашли число, то сначала это цена (по порядку)
                    if (price == 0) {
                        price = args[i].length() < 9 ? Float.parseFloat(args[i]) : Float.parseFloat(args[i].substring(0, 8));
                    }
                    // а второе число - это кол-во
                    else {
                        quantity = args[i].length() < 5 ? (int) Integer.parseInt(args[i]) : (int) Integer.parseInt(args[i].substring(0, 4));
                    }
                }
                //все НЕ числа - это название
                catch (Exception e) {
                    productName = productName.concat(args[i]).concat(" ");
                    if (productName.length() > 30) productName = productName.substring(0, 30);
                }
            }

            //читаем файл
            BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(fileName));
            byte[] bytes = new byte[inputStream.available()];
            inputStream.read(bytes);

            //ищем макс ID в 1-х 8 символах каждой строки
            int id = 0;
            String temp = "";
            for (int i = 0; i < bytes.length; i++) {
                while (temp.length() < 8) {
                    temp = temp.concat(String.valueOf((char) bytes[i]));
                    i++;
                }
                if (Integer.parseInt(temp.trim()) >= id) id = Integer.parseInt(temp.trim());
                temp = "";
                //перепрыгиваем до конца строки
                while (bytes[i] != 10) {
                    if (i == bytes.length - 1) break;
                    i++;
                }
            }
            id += 1;
            inputStream.close();

            //формируем строку для записи в виде массива байт
            String line = "";
            temp = String.valueOf(id);
            while (temp.length() < 8) { //делаем 8 символов в ID
                temp = temp.concat(" ");
            }
            line = line.concat(temp);

            while (productName.length() < 30) { //делаем 30 символов в названии
                productName = productName.concat(" ");
            }
            line = line.concat(productName);

            temp = String.valueOf(price);
            while (temp.length() < 8) { //делаем 8 символов в цене
                temp = temp.concat(" ");
            }
            line = line.concat(temp);

            temp = String.valueOf(quantity);
            while (temp.length() < 4) { //делаем 4 символа в кол-ве
                temp = temp.concat(" ");
            }
            line = line.concat(temp);

            //пишем в файл новую строку
            bytes = line.getBytes("windows-1251");
            BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(fileName, true));
            outputStream.write(13);
            outputStream.write(10);
            outputStream.write(bytes, 0, bytes.length);
            outputStream.close();
        }
    }
}