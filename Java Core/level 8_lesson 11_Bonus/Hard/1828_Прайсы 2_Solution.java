package com.javarush.task.task18.task1828;

/* 
Прайсы 2
*/

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws IOException {
        String file = new Scanner(System.in).nextLine();
        Scanner idreader = new Scanner(new FileInputStream(file));
        ArrayList<String> lines = new ArrayList<>();

        if (args[0].equals("-u")) {
            String id = args[1];
            StringBuilder newpname = new StringBuilder();
            StringBuilder newpprice = new StringBuilder();
            StringBuilder newpcount = new StringBuilder();

            while (idreader.hasNextLine()) {
                String s = idreader.nextLine();
                if (s.equals("0")) break;
                lines.add(s);
            }
            idreader.close();

            for (int i=0; i<lines.size(); i++) {
                String currid = lines.get(i).substring(0, 8);
                String trimmed = lines.get(i).substring(0, 8).trim();
                if (id.equals(trimmed)) {
                    newpname.append(args[2]);
                    if (args[2].length() <= 30) { for (int j=30-args[2].length(); j>0; j--) { newpname.append(" "); } }
                    newpprice.append(args[3]);
                    if (args[3].length() <= 8) { for (int j=8-args[3].length(); j>0; j--) { newpprice.append(" "); } }
                    newpcount.append(args[4]);
                    if (args[4].length() <= 4) { for (int j=4-args[4].length(); j>0; j--) { newpcount.append(" "); } }
                    String newpinfo = currid + newpname + newpprice + newpcount;
                    lines.set(i, newpinfo);
                }
            }

            PrintWriter cleaner = new PrintWriter(file);
            cleaner.flush();
            cleaner.close();

            FileOutputStream fos = new FileOutputStream(file, true);
            for (String t : lines) {
                fos.write(t.getBytes());
                fos.write(System.lineSeparator().getBytes());
            }
            fos.close();
        }

        if (args[0].equals("-d")) {
            String id = args[1];

            while (idreader.hasNextLine()) {
                String s = idreader.nextLine();
                if (s.equals("0")) break;
                lines.add(s);
            }
            idreader.close();

            for (int i=0; i<lines.size(); i++) {
                String trimmed = (lines.get(i).substring(0, 8)).trim();
                if (id.equals(trimmed)) {
                    lines.remove(i);
                }
            }

            PrintWriter cleaner = new PrintWriter(file);
            cleaner.flush();
            cleaner.close();

            FileOutputStream fos = new FileOutputStream(file, true);
            for (String t : lines) {
                fos.write(t.getBytes());
                fos.write(System.lineSeparator().getBytes());
            }
            fos.close();
        }
    }
}