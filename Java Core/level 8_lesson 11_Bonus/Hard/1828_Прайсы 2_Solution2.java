package com.javarush.task.task18.task1828;

/* 
Прайсы 2
*/

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        String fileName = scanner.nextLine();
        if (args.length != 0) {
            //запомнили введенный ID
            String id = args[1];
            //читаем файл в список строк
            List<String> lines = Files.readAllLines(Paths.get(fileName));
            //и открываем его для записи
            FileWriter fileWriter = new FileWriter(fileName);
            switch (args[0]) {
                case "-u": {
                    //из входной строки выделяем название, цену и кол-во с обрезанием длины
                    String productName = "";
                    String price = "";
                    String quantity = "";
                    for (int i = 2; i < args.length; i++) {
                        //если число
                        try {
                            //то сначала его пишем в цену
                            if (price.equals("")) {
                                price = String.valueOf(args[i].length() < 9 ? Float.parseFloat(args[i]) : Float.parseFloat(args[i].substring(0, 8)));
                            }
                            // а второе число в кол-во
                            else {
                                quantity = String.valueOf(args[i].length() < 5 ? (int) Integer.parseInt(args[i]) : (int) Integer.parseInt(args[i].substring(0, 4)));
                            }
                        }
                        //все НЕ числа клеим в название
                        catch (Exception e) {
                            productName = productName.concat(args[i]).concat(" ");
                            if (productName.length() > 30) productName = productName.substring(0, 30);
                        }
                    }

                    //создаем новую строку из новых параметров, доводя их до нужной длины
                    String newline = "";
                    while (id.length() < 8) {
                        id = id.concat(" ");
                    }
                    newline = newline.concat(id);
                    while (productName.length() < 30) { //делаем 30 символов в названии
                        productName = productName.concat(" ");
                    }
                    newline = newline.concat(productName);
                    while (price.length() < 8) { //делаем 8 символов в цене
                        price = price.concat(" ");
                    }
                    newline = newline.concat(price);
                    while (quantity.length() < 4) { //делаем 4 символа в кол-ве
                        quantity = quantity.concat(" ");
                    }
                    newline = newline.concat(quantity);

                    //меняем строку с нашим ID на новую
                    for (String line: lines) {
                        if (line.startsWith(args[1])){
                            lines.set(lines.indexOf(line),newline.trim());
                            break;
                        }
                    }
                }
                case "-d": {
                    //удаляем строку с нашим ID из списка
                    for (String line: lines) {
                        if (line.startsWith(args[1])){
                            lines.set(lines.indexOf(line),"deleted");
                            break;
                        }
                    }
                }
            }
            //пишем все строки в файл
            for (int i = 0; i < lines.size(); i++) {
                String temp = lines.get(i);
                if (!temp.equals("deleted")) {
                    if (temp.equals("")) fileWriter.write("\r\n");
                    else fileWriter.write(temp);
                }
                else i++;
            }
            fileWriter.close();
        }
        scanner.close();
    }
}