package com.javarush.task.task18.task1826;

/* 
Шифровка
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(args[1]));
        BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(args[2]));
        byte[] bytes = new byte[inputStream.available()];
        inputStream.read(bytes);

        if (args[0].equals("-e")){
            for(int i = 0; i < bytes.length; i++) {
                int b = bytes[i];
                bytes[i] = (byte)((b == 255) ? 0 : b+1);
            }
            outputStream.write(bytes);
        }
        else if (args[0].equals("-d")){
            for(int i = 0; i < bytes.length; i++) {
                int b = bytes[i];
                bytes[i] = (byte)((b == 0) ? 255 : b-1);
            }
            outputStream.write(bytes);
        }

        inputStream.close();
        outputStream.close();
    }

}