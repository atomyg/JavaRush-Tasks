package com.javarush.task.task18.task1821;

/* 
Встречаемость символов
*/

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(args[0]));
        byte[] array = new byte[inputStream.available()];
        inputStream.read(array);
        int[] ASCII = new int[256]; //все возможные коды ASCII
        //идем по массиву ARRAY и для каждого байта делаем +1 в соответствующей ячейке ASCII
        for (int i = 0; i < array.length; i++) {
            ASCII[array[i]]++;
        }
        //если символ встречался хоть раз, то выводим его и кол-во повторений
        for (int i = 0; i < ASCII.length; i++) {
            if (ASCII[i]>0) System.out.println((char)i+" "+ASCII[i]);
        }
        inputStream.close();
    }
}