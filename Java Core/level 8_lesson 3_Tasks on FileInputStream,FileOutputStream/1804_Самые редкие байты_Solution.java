package com.javarush.task.task18.task1804;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/* 
Самые редкие байты
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename = reader.readLine();
        FileInputStream fileInputStream = new FileInputStream(filename);
        Map<Integer, Integer> map = new HashMap<>();
        int min = Integer.MAX_VALUE;
        //если файл не пустой
        if (fileInputStream.available() > 0) {
            int bite; //считанный байт
            int count; //число повторений байта
            //записываем в MAP все байты и число повторений
            while (fileInputStream.available() > 0){
                bite = fileInputStream.read();
                if (map.containsKey(bite)) { //если такой байт уже есть, то увеличиваем число повторений
                    count = map.get(bite);
                    map.put(bite,++count);
                }
                else {
                    map.put(bite, 1);
                }
            }
            //ищем минимальное кол-во повторений
            for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
                if (entry.getValue() < min) min = entry.getValue();
            }
            //печатаем все байты из MAP, у которых кол-во повторений равно min
            String s = "";
            for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
                if (entry.getValue() == min) s += entry.getKey()+" ";
            }
            System.out.println(s.trim());
        }
        else System.out.println("Файл пустой");
        fileInputStream.close();
        reader.close();
    }
}