package com.javarush.task.task18.task1805;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.*;

/* 
Сортировка байт
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename = reader.readLine();
        FileInputStream fileInputStream = new FileInputStream(filename);
        Set<Integer> list = new TreeSet<>();
        if (fileInputStream.available() > 0){
            while (fileInputStream.available() > 0) {
                list.add(fileInputStream.read());
            }
            String s = "";
            for (int i:list) {
                s = s.concat(""+i+" ");
            }
            System.out.println(s.trim());
        }
        else{
            System.out.println("Файл пустой");
        }
        reader.close();
        fileInputStream.close();
    }
}