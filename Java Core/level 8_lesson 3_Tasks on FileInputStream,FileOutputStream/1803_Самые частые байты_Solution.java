package com.javarush.task.task18.task1803;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/* 
Самые частые байты
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename = reader.readLine();
        FileInputStream fileInputStream = new FileInputStream(filename);
        Map<Integer, Integer> map = new HashMap<>();
        //если файл не пустой
        if (fileInputStream.available() > 0) {
            int max = 0;
            int bite; //считанный байт
            int count; //число повторений байта
            //записываем в MAP все байты и число повторений
            while (fileInputStream.available() > 0){
                bite = fileInputStream.read();
                if (map.containsKey(bite)) { //если такой байт уже есть, то увеличиваем число повторений
                    count = map.get(bite);
                    map.put(bite,++count);
                    if (map.get(bite) > max) max = map.get(bite); //обновляем макс число повторений из всех
                }
                else map.put(bite, 1);
            }
            //печатаем все байты из MAP, у которых кол-во повторений равно max
            String s = "";
            for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
                if (entry.getValue() == max) s += entry.getKey()+" ";
            }
            System.out.println(s.trim());
        }
        else System.out.println("Файл пустой");
        fileInputStream.close();
        reader.close();
    }
}