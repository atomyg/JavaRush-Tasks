package com.javarush.task.task18.task1802;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

/* 
Минимальный байт
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename = reader.readLine();
        FileInputStream fileInputStream = new FileInputStream(filename);
        int min;
        if (fileInputStream.available() > 0) {
            min = fileInputStream.read();
            while (fileInputStream.available() > 0){
                int temp = fileInputStream.read();
                if (temp < min) min = temp;
            }
            System.out.println(min);
        }
        else System.out.println("Файл пустой");
        fileInputStream.close();
        reader.close();
    }
}