package com.javarush.task.task17.task1721;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/* 
Транзакционность
*/

public class Solution {
    public static List<String> allLines = new ArrayList<String>();
    public static List<String> forRemoveLines = new ArrayList<String>();

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Path f1 = Paths.get(reader.readLine());
        Path f2 = Paths.get(reader.readLine());
        for (String st1 : Files.readAllLines(f1, Charset.forName("Windows-1251"))) {
            allLines.add(st1);
        }
        for (String st2 : Files.readAllLines(f2, Charset.forName("Windows-1251"))) {
            forRemoveLines.add(st2);
        }
        try {
            new Solution().joinData();
        } catch (Exception e) {
            System.out.println("Исключение");
        }
        reader.close();
    }

    public void joinData () throws CorruptedDataException {
        if (allLines.containsAll(forRemoveLines)) {
            allLines.removeAll(forRemoveLines);
        }
        else {
            allLines.clear();
            throw new CorruptedDataException();
        }
    }
}
