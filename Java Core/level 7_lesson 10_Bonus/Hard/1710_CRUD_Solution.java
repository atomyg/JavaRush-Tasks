package com.javarush.task.task17.task1710;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/* 
CRUD
*/

public class Solution {
    public static List<Person> allPeople = new ArrayList<Person>();

    static {
        allPeople.add(Person.createMale("Иванов Иван", new Date()));  //сегодня родился    id=0
        allPeople.add(Person.createMale("Петров Петр", new Date()));  //сегодня родился    id=1
    }

    public static void main(String[] args) throws ParseException {
        //Создаем переменные для удобства и сокращения кода
        int id = 0;
        String name = null;
        Sex sex = null;
        Date birthday = null;
        SimpleDateFormat indate = new SimpleDateFormat("dd/MM/yyyy",Locale.ENGLISH); //формат даты при вводе
        SimpleDateFormat outdate = new SimpleDateFormat("dd-MMM-yyyy",Locale.ENGLISH); //формат даты при выводе

        //Обрабатываем команду ввода
        if (args[0].equals("-c")){
            name = args[1];
            sex = args[2].equals("м") ? Sex.MALE : Sex.FEMALE;
            birthday = indate.parse(args[3]);
            allPeople.add(sex.equals(Sex.MALE)? Person.createMale(name, birthday) : Person.createFemale(name, birthday));
            System.out.println(allPeople.size()-1);
        }
        if (args[0].equals("-u")) {
            id = Integer.parseInt(args[1]);
            name = args[2];
            sex = args[3].equals("м") ? Sex.MALE : Sex.FEMALE;
            birthday = indate.parse(args[4]);
            allPeople.get(id).setName(name);
            allPeople.get(id).setSex(sex);
            allPeople.get(id).setBirthDay(birthday);
        }
        if (args[0].equals("-d")) {
            id = Integer.parseInt(args[1]);
            allPeople.get(id).setBirthDay(null);
            allPeople.get(id).setName(null);
            allPeople.get(id).setSex(null);
        }
        if (args[0].equals("-i")) {
            Person p = allPeople.get(Integer.parseInt(args[1]));
            System.out.println(p.getName()+" "+(p.getSex()==Sex.MALE ? "м" : "ж")+" "+outdate.format(p.getBirthDay()));
        }
    }
}