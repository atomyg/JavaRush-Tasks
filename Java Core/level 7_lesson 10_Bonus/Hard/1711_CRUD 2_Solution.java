package com.javarush.task.task17.task1711;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/* 
CRUD 2
*/

public class Solution {
    public static volatile List<Person> allPeople = new ArrayList<Person>();

    static {
        allPeople.add(Person.createMale("Иванов Иван", new Date()));  //сегодня родился    id=0
        allPeople.add(Person.createMale("Петров Петр", new Date()));  //сегодня родился    id=1
    }

    public static void main(String[] args) throws ParseException {
        SimpleDateFormat indate = new SimpleDateFormat("dd/MM/yyyy",Locale.ENGLISH); //формат даты при вводе
        SimpleDateFormat outdate = new SimpleDateFormat("dd-MMM-yyyy",Locale.ENGLISH); //формат даты при выводе

        switch (args[0]) {
            case "-c": synchronized (allPeople) {
                int n = 3; //кол-во параметров на 1 персону
                for (int i = 0; i < (args.length - 1) / n; i++) {
                    String name = args[1 + n * i];
                    Sex sex = args[2 + n * i].equals("м") ? Sex.MALE : Sex.FEMALE;
                    Date birthday = indate.parse(args[3 + n * i]);
                    allPeople.add(sex.equals(Sex.MALE) ? Person.createMale(name, birthday) : Person.createFemale(name, birthday));
                    System.out.println(allPeople.size() - 1);
                }
                break;
            }
            case "-u": synchronized (allPeople) {
                int n = 4; //кол-во параметров на 1 персону
                for (int i = 0; i < (args.length - 1) / n; i++) {
                    int id = Integer.parseInt(args[1 + n * i]);
                    allPeople.get(id).setName(args[2 + n * i]);
                    allPeople.get(id).setSex(args[3 + n * i].equals("м") ? Sex.MALE : Sex.FEMALE);
                    allPeople.get(id).setBirthDay(indate.parse(args[4 + n * i]));
                }
                break;
            }
            case "-d": synchronized (allPeople) {
                for (int i = 0; i < (args.length-1); i++) {
                    int id = Integer.parseInt(args[1+i]);
                    allPeople.get(id).setBirthDay(null);
                    allPeople.get(id).setName(null);
                    allPeople.get(id).setSex(null);
                }
                break;
            }
            case "-i": synchronized (allPeople) {
                for (int i = 0; i < (args.length-1); i++) {
                    int id = Integer.parseInt(args[1 + i]);
                    Person p = allPeople.get(Integer.parseInt(args[1+i]));
                    System.out.println(p.getName() + " " + (p.getSex() == Sex.MALE ? "м" : "ж") + " " + outdate.format(p.getBirthDay()));
                }
                break;
            }
        }
    }
}
