package com.javarush.task.task19.task1908;

/* 
Выделяем числа
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedReader reader = new BufferedReader(new FileReader(bufferedReader.readLine()));
        BufferedWriter writer = new BufferedWriter(new FileWriter(bufferedReader.readLine()));
        bufferedReader.close();
        String s = "";
        while ((s = reader.readLine()) != null){
            for (String ss : s.split("\\s")) {
                if (ss.matches("\\d+")) writer.write(ss.concat(" "));
            }
        }
        reader.close();
        writer.close();
    }
}