package com.javarush.task.task19.task1910;

/* 
Пунктуация
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String f1 = reader.readLine(), f2 = reader.readLine();
        reader.close();
        reader = new BufferedReader(new FileReader(f1));
        BufferedWriter writer = new BufferedWriter(new FileWriter(f2));
        String line = "";
        while ((line = reader.readLine())!= null){
            writer.write(line.replaceAll("\\p{Punct}",""));
        }
        reader.close();
        writer.close();
    }
}