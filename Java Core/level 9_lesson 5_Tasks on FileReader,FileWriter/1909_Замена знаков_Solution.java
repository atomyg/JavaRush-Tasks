package com.javarush.task.task19.task1909;

/* 
Замена знаков
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filName1 = reader.readLine();
        String filName2 = reader.readLine();
        reader.close();
        reader = new BufferedReader(new FileReader(filName1));
        BufferedWriter writer = new BufferedWriter(new FileWriter(filName2));
        String line = "";
        while ((line = reader.readLine())!= null){
            line = line.replace('.','!');
            writer.write(line);
        }
        reader.close();
        writer.close();
    }
}