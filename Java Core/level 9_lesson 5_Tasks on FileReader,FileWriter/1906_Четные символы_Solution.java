package com.javarush.task.task19.task1906;

/* 
Четные символы
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String fileName1 = bufferedReader.readLine();
        String fileName2 = bufferedReader.readLine();
        bufferedReader.close();
        FileReader reader = new FileReader(fileName1);
        FileWriter writer = new FileWriter(fileName2);
        int number = 0, symbol;
        while ((symbol = reader.read()) != -1){
            if (++number % 2 == 0) writer.write(symbol);
        }
        reader.close();
        writer.close();
    }
}
