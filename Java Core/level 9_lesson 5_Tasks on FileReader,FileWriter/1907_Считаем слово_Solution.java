package com.javarush.task.task19.task1907;

/* 
Считаем слово
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedReader reader = new BufferedReader(new FileReader(bufferedReader.readLine()));
        bufferedReader.close();
        int count = 0;
        String line;
        while ((line = reader.readLine()) != null) {
            for (String str: line.split("\\W")) {
                if (str.equals("world")) count++;
            }
        }
        System.out.println(count);
        reader.close();
    }
}