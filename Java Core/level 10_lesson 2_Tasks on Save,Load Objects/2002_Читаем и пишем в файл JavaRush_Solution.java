package com.javarush.task.task20.task2002;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/* 
Читаем и пишем в файл: JavaRush
*/
public class Solution {
    public static void main(String[] args) {
        //you can find your_file_name.tmp in your TMP directory or fix outputStream/inputStream according to your real file location
        //вы можете найти your_file_name.tmp в папке TMP или исправьте outputStream/inputStream в соответствии с путем к вашему реальному файлу
        try {
            File your_file_name = File.createTempFile("your_file_name", null);
            OutputStream outputStream = new FileOutputStream(your_file_name);
            InputStream inputStream = new FileInputStream(your_file_name);

            JavaRush javaRush = new JavaRush();
            //initialize users field for the javaRush object here - инициализируйте поле users для объекта javaRush тут
            User u1 = new User();
            u1.setLastName("ivanov");
            u1.setFirstName("petr");
            u1.setBirthDate(new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.S").parse("01/01/1990 04:12:32.4"));
            u1.setMale(true);
            u1.setCountry(User.Country.RUSSIA);
            javaRush.users.add(u1);
            User u2 = new User();
            u2.setLastName("sidorov");
            u2.setFirstName("kolun");
            u2.setBirthDate(new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.S").parse("21/06/1845 10:43:56.2"));
            u2.setMale(true);
            u2.setCountry(User.Country.UKRAINE);
            javaRush.users.add(u1);
            javaRush.save(outputStream);
            outputStream.flush();

            JavaRush loadedObject = new JavaRush();
            loadedObject.load(inputStream);
            //check here that javaRush object equals to loadedObject object - проверьте тут, что javaRush и loadedObject равны
            System.out.println(loadedObject.equals(javaRush));


            outputStream.close();
            inputStream.close();

        } catch (IOException e) {
            //e.printStackTrace();
            System.out.println("Oops, something wrong with my file");
        } catch (Exception e) {
            //e.printStackTrace();
            System.out.println("Oops, something wrong with save/load method");
        }
    }

    public static class JavaRush {
        public List<User> users = new ArrayList<>();

        public void save(OutputStream outputStream) throws Exception {
            PrintWriter writer = new PrintWriter(outputStream);
            if (users.size() == 0)
                writer.println("No users");
            else {
                writer.println("Have users");
                for (User user : users)
                    writer.write(user.getFirstName()+","+user.getLastName()+","+new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.S").format(user.getBirthDate())+","+
                            user.isMale()+","+user.getCountry()+"\n");
            }
            writer.flush();
        }

        public void load(InputStream inputStream) throws Exception {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String status = reader.readLine();
            if (status.equals("No users"))
                users = new ArrayList<>();
            else if (status.equals("Have users")) {
                String line;
                while ((line = reader.readLine())!= null) {
                    if (line.equals("")) continue;
                    String[] data = line.split(",");
                    User user = new User();
                    user.setFirstName(data[0]);
                    user.setLastName(data[1]);
                    user.setBirthDate(new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.S").parse(data[2]));
                    user.setMale(Boolean.parseBoolean(data[3]));
                    user.setCountry(User.Country.valueOf(data[4]));
                    users.add(user);
                }
            }
            else System.out.println("Wrong load file data");
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            JavaRush javaRush = (JavaRush) o;

            return users != null ? users.equals(javaRush.users) : javaRush.users == null;

        }

        @Override
        public int hashCode() {
            return users != null ? users.hashCode() : 0;
        }
    }
}