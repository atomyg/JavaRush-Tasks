package com.javarush.task.task18.task1809;

/* 
Реверс файла
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream fileInputStream = new FileInputStream(reader.readLine());
        FileOutputStream fileOutputStream = new FileOutputStream(reader.readLine());

        byte[] array = new byte[fileInputStream.available()];
        fileInputStream.read(array);    //загоняем все байты 1-го файла разом в 1-й массив

        byte[] reverse = new byte[array.length];    //создаем 2-й массив такой же длины
        int n = 0;  //номер элемента 2-го массива
        for (int i = array.length-1; i > -1; i--) {
            reverse[n++] = array[i];    //идем с конца 1-го массива и пишем элементы в начало 2-го
        }
        fileOutputStream.write(reverse);    //заливаем 2-й массив во 2-й файл

        reader.close();
        fileInputStream.close();
        fileOutputStream.close();
    }
}