package com.javarush.task.task18.task1808;

/* 
Разделение файла
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filename1 = reader.readLine();
        String filename2 = reader.readLine();
        String filename3 = reader.readLine();
        FileInputStream fileInputStream = new FileInputStream(filename1);
        FileOutputStream fileOutputStream1 = new FileOutputStream(filename2);
        FileOutputStream fileOutputStream2 = new FileOutputStream(filename3);

        int size = (fileInputStream.available()+1)/2; //ровно половина файла или +1 первая часть
        byte[] arrbite = new byte[size];    //размер массива под 1-й кусок
        fileInputStream.read(arrbite);  //читаем 1-й кусок
        fileOutputStream1.write(arrbite);    //записываем в 1-й файл

        size = fileInputStream.available(); //оставшиеся непрочитанные байты
        arrbite = new byte[size];   //новый размер массива под 2-й кусок
        fileInputStream.read(arrbite);  //читаем 2-й кусок
        fileOutputStream2.write(arrbite);   //записываем во 2-й файл

        reader.close();
        fileInputStream.close();
        fileOutputStream1.close();
        fileOutputStream2.close();
    }
}