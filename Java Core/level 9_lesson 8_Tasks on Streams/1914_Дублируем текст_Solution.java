package com.javarush.task.task19.task1914;

/* 
Решаем пример
*/

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class Solution {
    public static TestString testString = new TestString();

    public static void main(String[] args) {
        PrintStream out = System.out;
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(outputStream);
        System.setOut(printStream);
        testString.printSomething();
        System.setOut(out);
        String s = outputStream.toString().replaceAll("\r\n","");
        String[] str = s.split(" ");
        int d1 = Integer.parseInt(str[0]);
        int d2 = Integer.parseInt(str[2]);
        switch (str[1]) {
            case "+": System.out.println(s+String.valueOf(d1+d2)); break;
            case "-": System.out.println(s+String.valueOf(d1-d2)); break;
            case "*": System.out.println(s+String.valueOf(d1*d2)); break;
        }
    }

    public static class TestString {
        public void printSomething() {
            System.out.println("3 + 6 = ");
        }
    }
}