package com.javarush.task.task19.task1920;

/* 
Самый богатый
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(args[0]));
        Map<String, Double> zpMap = new TreeMap<>();
        String line;
        String[] s;
        double maxZP = 0;
        while((line = reader.readLine()) != null) {
            s = line.trim().split("\\s+");
            if (zpMap.containsKey(s[0])) {
                zpMap.put(s[0], Double.parseDouble(s[1])+zpMap.get(s[0]));
            }
            else zpMap.put(s[0], Double.parseDouble(s[1]));
            maxZP = zpMap.get(s[0]) > maxZP ? zpMap.get(s[0]) : maxZP;
        }

        for (Map.Entry<String, Double> entry : zpMap.entrySet()) {
            if (entry.getValue() == maxZP) System.out.println(entry.getKey());
        }
        reader.close();
    }
}