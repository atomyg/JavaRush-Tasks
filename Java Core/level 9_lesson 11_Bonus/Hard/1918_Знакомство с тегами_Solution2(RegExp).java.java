package com.javarush.task.task19.task1918;

/* 
Знакомство с тегами
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String fileName = "c:\\1\\h.html";
        reader.close();
        reader = new BufferedReader(new FileReader(fileName));
        StringBuffer sb = new StringBuffer();
        String line;
        while ((line = reader.readLine()) != null){
            sb.append(line);
        }
        reader.close();

        // ищем все открывающие тэги и запоминаем их индексы расположения в строке
        List<Integer> openList = new ArrayList<>();
        String openTag = "<"+args[0];
        Pattern p = Pattern.compile(openTag);
        Matcher m = p.matcher(sb);
        while (m.find()) {
            openList.add(m.start());
        }

        // ищем все закрывающие тэги и запоминаем их индексы расположения в строке
        List<Integer> closeList = new ArrayList<>();
        String closeTag = "</"+args[0]+">";
        p = Pattern.compile(closeTag);
        m = p.matcher(sb);
        while (m.find()) {
            closeList.add(m.end()-1);
        }

        // берем первый openTag и идем до closeTag, считаю openTag-и по пути (уровень вложенности)
        int closeID, openID, level = 0;
        while (openList.size() != 0) {
            for (int i = 0; i < openList.size(); i++) {
                if (openList.get(i) < closeList.get(0)) level++;
                else break;
            }
            for (int i = level-1; i >= 0; i--) {
                openID = openList.get(0);
                closeID = closeList.get(i);
                line = sb.substring(openID, closeID);
                System.out.println(line);
                openList.remove(0);
                closeList.remove(i);
                level = 0;
                break;
            }
        }
    }
}