package com.javarush.task.task19.task1919;

/* 
Считаем зарплаты
*/

import java.io.*;
import java.util.Map;
import java.util.TreeMap;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(args[0]));
        Map<String, Double> zpMap = new TreeMap<>();
        String line;
        String[] s;
        while ((line = reader.readLine()) != null) {
            s = line.trim().split("\\s+");
            if (zpMap.containsKey(s[0])){
                zpMap.put(s[0], zpMap.get(s[0])+Double.parseDouble(s[1]));
            }
            else {
                zpMap.put(s[0], Double.parseDouble(s[1]));
            }

        }
        for (Map.Entry<String, Double> entry: zpMap.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }
        reader.close();
    }
}