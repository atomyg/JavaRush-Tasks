package com.javarush.task.task19.task1921;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/* 
Хуан Хуанович
*/

public class Solution {
    public static final List<Person> PEOPLE = new ArrayList<Person>();

    public static void main(String[] args) throws IOException, ParseException {
        BufferedReader reader = new BufferedReader(new FileReader(args[0]));
        Date birthday;
        SimpleDateFormat format = new SimpleDateFormat("dd MM yyyy");
        String line;
        while ((line = reader.readLine()) != null){
            if (line.equals("")) continue;
            String name = line.trim().split("(\\s+\\d+){3}")[0];
            String birth = line.substring(line.indexOf(name)+name.length()).trim();
            birthday = format.parse(birth);
            PEOPLE.add(new Person(name, birthday));
        }
        reader.close();
    }
}