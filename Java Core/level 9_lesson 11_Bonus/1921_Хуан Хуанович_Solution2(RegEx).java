package com.javarush.task.task19.task1921;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* 
Хуан Хуанович
*/

public class Solution2 {
    public static final List<Person> PEOPLE = new ArrayList<Person>();

    public static void main(String[] args) throws IOException, ParseException {
        BufferedReader reader = new BufferedReader(new FileReader(args[0]));
        Pattern p = Pattern.compile("(\\D*)\\s+(\\d{1,2})\\s+(\\d{1,2})\\s+(\\d{4})");
        Matcher m;
        Date birthday = null;
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        String line;
        while ((line = reader.readLine()) != null){
            if (line.equals("")) continue;
            m = p.matcher(line);
            if (m.find()) {
                birthday = format.parse(m.group(2)+"/"+m.group(3)+"/"+m.group(4));
                PEOPLE.add(new Person(m.group(1), birthday));
            }
        }
        reader.close();
    }
}