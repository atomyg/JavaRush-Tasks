package com.javarush.task.task19.task1904;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;

/* 
И еще один адаптер
*/

public class Solution {
    public static void main(String[] args) throws IOException {}

    public static class PersonScannerAdapter implements PersonScanner{
        private Scanner fileScanner;
        public PersonScannerAdapter(Scanner scanner){
            this.fileScanner = scanner;
        }

        @Override
        public Person read() throws IOException {
            Person p = null;
            if (this.fileScanner.hasNextLine()) {
                String line = this.fileScanner.nextLine();
                String[] s = line.split(" ");
                String d = s[3].concat(" ").concat(s[4]).concat(" ").concat(s[5]);
                SimpleDateFormat df = new SimpleDateFormat("dd MM yyyy");
                try {
                    p = new Person(s[1],s[2],s[0],df.parse(d));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            return p;
        }

        @Override
        public void close() throws IOException {
            this.fileScanner.close();
        }
    }
}